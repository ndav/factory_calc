# Factory Calc

A calculator/generator for [Factory Idle](http://www.factoryidle.com) production lines. Generates a list of required machines for a given production target with given upgrade levels (rate, efficiency, and thrift, for lack of a better term).

Future versions will contain a web-based GUI.

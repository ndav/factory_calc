from setuptools import setup

setup(name='factory_calc',
      version='0.1',
      description='Perform various cost/quantity calculations for Factory Idle game',
      url='http://www.tty72.com/factorycalc/',
      download_url='https://gitlab.com/ndav/factory_calc',
      author='tty72',
      author_email='admin@tty72.com',
      license='GPLv3',
      packages=['factory_calc'],
#      package_data= {'honeydsql': ['sql/*']},
#      include_package_data=True,
      install_requires = [
          'enum34',
          'm3-cdecimal',
      ],
#      entry_points={
#          'console_scripts': ['honeydsql=honeydsql.honeydsql:main']
#      },
      zip_safe=False)

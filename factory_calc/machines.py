import collections
from enum import Enum
from cdecimal import Decimal as D

# Price - Cost of machine. Cost - Running cost per tick. Cost_reduce - list of running cost reduction tiers.
# Efficiency - Increase in production w/o increase in resources. Rate - Increase in throughput (input and output).
# Inputs - List of input types. Output - List of output types. Unit_cost - cost per unit (producers are non-zero)
prod_vals = collections.namedtuple('Production',
                                   ['price', 'cost', 'thrift', 
                                    'efficiency', 'rate',
                                    'inputs', 'outputs'])

class Resources(Enum):
    """Enumerated list of resources."""
    ironOre = 0
    coal = 1
    oil = 2
    gas = 3
    silicon = 4
    explosive = 5
    aluminum = 6
    waste = 7
    iron = 16
    steel = 17
    plastic = 18
    electronic = 19
    bullet = 20
    gun = 21
    engine = 22
    tankHull = 23
    tankTurret = 24
    tank = 25
    diesel = 26
    rocketFuel = 27
    rocketHull = 28
    rocketWarhead = 29
    rocket = 30
    ironSale = 32
    steelSale = 33
    plasticSale = 34
    electronicSale = 35
    gunSale = 36
    engineSale = 37
    tankSale = 38
    rocketSale = 39
    cash = 64
    research = 65


mtypes = {
    Resources.ironOre: prod_vals(D('50.0'), D('0.0'),
                                  [D('1.0')], [D('1.0')],
                                  [1, 2, 4, 8, 12, 24, 36, 48, 64],
                                  [(Resources.cash, D('0.0'))],
                                  [(Resources.ironOre, D('0.1'))]),
    Resources.coal: prod_vals(D('1000.00'), D('1.00'),
                              [D('1.0'), D('0.95'), D('0.9')], [D('1.0')],
                              [1, 2, 4, 8, 12, 18, 24],
                              [(Resources.cash, D('0.25'))],
                              [(Resources.coal, D('0.2'))]),
    Resources.oil: prod_vals(D('40000.0'), D('8.0'),
                             [D('1.0'), D('0.95'), D('0.9')], [D('1.0')],
                             [1, 2, 8, 12, 24],
                             [(Resources.cash, D('20.0'))],
                             [(Resources.oil, D('0.1'))]),
    Resources.gas: prod_vals(D('60000.0'), D('6.0'),
                             [D('1.0'), D('0.95'), D('0.9')], [D('1.0')],
                             [1, 2, 4, 8, 12, 24],
                             [(Resources.cash, D('8.0'))],
                             [(Resources.gas, D('0.1'))]),
    Resources.silicon: prod_vals(D('50000000.0'), D('120.0'),
                                 [D('1.0'), D('0.95')], [D('1.0')], 
                                 [1, 2, 3, 6, 12],
                                 [(Resources.cash, D('40.0'))], 
                                 [(Resources.silicon, D('0.1'))]),
    Resources.explosive: prod_vals(D('500.0e6'), D('1900.0'),
                                    [D('1.0'), D('0.95')], [D('1.0')], 
                                    [1, 2, 4, 8], 
                                    [(Resources.cash, D('45.0'))], 
                                    [(Resources.explosive, D('0.05'))]),
    Resources.aluminum: prod_vals(D('3.0e9'), D('48000.0'),
                                  [D('1.0'), D('0.95'), D('0.9')], [D('1.0')], 
                                  [1, 2, 4, 8, 32],
                                  [(Resources.cash, D('140.0'))], 
                                  [(Resources.aluminum, D('0.1'))]), 
    Resources.iron: prod_vals(D('150.0'), D('0.0'),
                              [D('1.0')], [D('1.0')], 
                              [1, 2, 4, 6, 12, 18, 24, 48],
                              [(Resources.ironOre, D('0.2'))],
                              [(Resources.iron, D('0.1'))]),
    Resources.steel: prod_vals(D('4000.0'), D('4.0'),
                               [D('1.0'), D('0.95'), D('0.85')], [D('1.0'), D('2.0'), D('4.0')],
                               [1, 2, 3, 6, 12, 24],
                               [(Resources.iron, D('0.4')),
                                (Resources.coal, D('0.4'))],
                               [(Resources.steel, D('0.1'))]),
    Resources.plastic: prod_vals(D('240000.0'), D('20.0'),
                                 [D('1.0'), D('0.95'), D('0.85')], [D('1.0'), D('2.0')],
                                 [1, 2, 3, 6],
                                 [(Resources.coal, D('0.4')),
                                  (Resources.oil, D('0.4')),
                                  (Resources.gas, D('0.2'))],
                                 [(Resources.plastic, D('0.1')),
                                  (Resources.waste, D('0.1'))]),
    Resources.electronic: prod_vals(D('200000000.0'), D('300.0'),
                                    [D('1.0'), D('0.95')], [D('1.0'), D('2.0')],
                                    [1, 2, 3],
                                    [(Resources.silicon, D('0.2')),
                                     (Resources.plastic, D('0.4'))],
                                    [(Resources.electronic, D('0.1')),
                                     (Resources.waste, D('0.1'))]),
    Resources.bullet: prod_vals(D('800000000.0'), D('3900.0'),
                                [D('1.0'), D('0.9')], [D('1.0')],
                                [1, 2, 4, 8],
                                [(Resources.steel, D('0.15')),
                                 (Resources.explosive, D('0.1'))],
                                [(Resources.bullet, D('0.1'))]),
    Resources.gun: prod_vals(D('1.0e9'), D('5900.0'),
                             [D('1.0'), D('0.9')], [D('1.0')],
                             [1, 2, 8, 16],
                             [(Resources.steel, D('0.15')),
                              (Resources.bullet, D('0.1'))],
                             [(Resources.gun, D('0.1'))]),
    Resources.engine: prod_vals(D('32.0e9'), D('120000.0'),
                                [D('1.0'), D('0.9'), D('0.8')], [D('1.0')],
                                [1, 2, 4],
                                [(Resources.aluminum, D('0.4')),
                                 (Resources.steel, D('0.6')),
                                 (Resources.electronic, D('0.3'))],
                                [(Resources.engine, D('0.1')),
                                 (Resources.waste, D('0.1'))]),
    Resources.tankHull: prod_vals(D('400.0e9'), D('1.6e6'),
                                  [1, D('0.95'), D('0.9')], [D('1.0')],
                                  [1, 4, 8],
                                  [(Resources.electronic, D('0.3')),
                                   (Resources.steel, D('0.6')),
                                   (Resources.aluminum, D('0.2'))],
                                  [(Resources.tankHull, D('0.1'))]),
    Resources.tankTurret: prod_vals(D('800.0e9'), D('2.0e6'),
                                    [D('1.0'), D('0.95'), D('0.9')], [D('1.0')],
                                    [1, 4, 8],
                                    [(Resources.gun, D('0.4')),
                                     (Resources.steel, D('0.6'))],
                                    [(Resources.tankTurret, D('0.1'))]),
    Resources.tank: prod_vals(D('700.0e9'), D('1.8e6'),
                              [D('1.0'), D('0.95'), D('0.9')], [D('1.0')],
                              [1, 2, 4],
                              [(Resources.tankHull, D('0.1')),
                               (Resources.tankTurret, D('0.1')),
                               (Resources.engine, D('0.1'))],
                              [(Resources.tank, D('0.1'))]),
    Resources.ironSale: prod_vals(D('100.0'), D('0.0'),
                                  [D('1.0')], [D('1.0'), D('1.25'), D('1.5')],
                                  [1, 2, 4],
                                  [(Resources.iron, D('0.2'))],
                                  [(Resources.cash, D('0.5'))]),
    Resources.steelSale: prod_vals(D('1500.0'), D('1.0'),
                                   [D('1.0'), D('0.95'), D('0.85')], [D('1.0'), D('1.1'), D('1.2')],
                                   [1, 2, 3],
                                   [(Resources.steel, D('0.2'))],
                                   [(Resources.cash, D('27.2'))]),
    Resources.plasticSale: prod_vals(D('180000.0'), D('12.0'),
                                     [D('1.0'), D('0.95'), D('0.9')], [D('1.0'), D('1.1'), D('1.15')],
                                     [1, 4, 6],
                                     [(Resources.plastic, D('0.1'))],
                                     [(Resources.cash, D('281.6'))]),
    Resources.electronicSale: prod_vals(D('80.0e6'), D('90.0'),
                                        [D('1.0'), D('0.95')], [D('1.0'), D('1.05'), D('1.1'), D('1.15'), D('1.2')],
                                        [1, 2, 3],
                                        [(Resources.electronic, D('0.1'))],
                                        [(Resources.cash, D('4234.6'))]),
    Resources.gunSale: prod_vals(D('400.0e6'), D('1100.0'),
                                 [D('1.0'), D('0.9')], [D('1.0'), D('1.05')],
                                 [1, 2],
                                 [(Resources.gun, D('0.1'))],
                                 [(Resources.cash, D('45923.4'))]),
    Resources.engineSale: prod_vals(D('8.0e9'), D('80000.0'),
                                    [D('1.0'), D('0.9')], [D('1.0'), D('1.05')],
                                    [1, 2],
                                    [(Resources.engine, D('0.1'))],
                                    [(Resources.cash, D('1.229e6'))]),
    Resources.tankSale: prod_vals(D('500.0e9'), D('2.4e6'),
                                  [D('1.0'), D('0.95'), D('0.9')], [D('1.0'), D('1.1'), D('1.2')], #VERIFY EFFICIENCY
                                  [1, 4, 8],
                                  [(Resources.tank, D('0.1'))],
                                  [(Resources.cash, D('25.642e6'))]),
    
                                  
                             }
# Base outputs    
# Iron - 1    
# Coal - 2
# Oil - 1
# Gas - 1

# Efficiency affects both resources out and running cost

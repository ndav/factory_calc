""" Automatically furnish a Factory Idle production line.

An object that generates a production line for a given production target,
optionally given upgrade modifiers for the various machines which may
be used in the production line. The object automatically calculates
required resources and populates the line with the appropriate number
of production machines necessary to meet (or exceed) those requirements.

Example Usage:
>>> from factory_idle import Line
>>> from machines import Resources
>>> 
>>> l = Line()
>>> l.target = Resources.iron
>>> l.machines
[(<Resources.ironOre: 0>, 2), (<Resources.iron: 16>, 1)]
>>> l.resources
{<Resources.ironOre: 0>: Decimal('0.00'), <Resources.iron: 16>: Decimal('0.10')}
>>> l.required(Resources.ironOre)
Decimal('0.2')

The furnishing algorithm is somewhat brute-force, so cdecimal is recommended.
However, cdecimal may be replaced with the standard decimal package if speed
is not an issue.

All returned production values are per-tick - if an iron buyer produces 4
iron ore every ten ticks, the values here will be 0.4.

A helper function (report) is also provided which returns a formatted string.
"""
from cdecimal import Decimal as D

class Line(object):
    """Construct a production line suitable for factoryidle.com."""
    _efficiency_levels = {}
    _thrift_levels = {}
    _rate_levels = {}

    def __init__(self, target=None, data_source=None):
        """ Initialize the production line.

        <target> may be specified at initialization, but will furnish the line
                 with all machine upgrades set at 0.
        <data_source>, if provided, should be an object which provides both an 
                       enumeration of resources, and a lookup mapping which
                       provides the production values for the machines. If no
                       data_source is provided, the included machines.py is
                       used by default.
        """
        if data_source is None:
            import machines as m
            self.ds=m
        else:
            self.ds = data_source
        self.expense = D('0.0')
        self.profit = D('0.0')
        self._machines = list()
        self.resources = dict()
        if target is not None:
            self.target = target

    @property
    def target(self):
        """Return the current production target or None if no target exists."""
        return getattr(self,'_target', None)

    @target.setter
    def target(self, val):
        """Set the production target and furnish the production line."""
        if val not in self.ds.Resources:
            raise KeyError('{} is not a valid resource'.format(val))
        self._target = val
        self.furnish()

    def _set_level(self, which, target, level):
        """Set the modifier level for target resource to level. 

        <which> is a string that selects the modifier to be set, and is one of 
                'efficiency', 'thrift', or 'rate'.
        <target> is a value from data_source.Resources enum.
        <level> will be clamped to the minimum (0) or maximum possible for that
                machine.

        Returns the actual level set after clamping.
        """
        if target not in self.ds.Resources:
            raise KeyError('{} is not a valid resource'.format(target))
        level = min(len(getattr(self.ds.mtypes[target], which)), max(level, 0))
        getattr(self, '_{}_levels'.format(which))[target] = level
        if self.target is not None:
            self.furnish()
        return level
        
    def set_efficiency(self, target, level):
        """ Set the efficiency level of <target> to <level>."""
        return self._set_level('efficiency', target, level)
    
    def set_thrift(self, target, level):
        """Set the thrift level of <target> to <level>."""
        return self._set_level('thrift', target, level)

    def set_rate(self, target, level):
        """Set the rate level of <target> to <level>."""
        return self._set_level('rate', target, level)

    def set_levels(self, level_dict):
        """Set multiple machine multiplier levels from a mapping."""
        for which in ['efficiency', 'thrift', 'rate']:
            levels = level_dict.get(which, {})
            for (target, level) in levels.items():
                self._set_level(which, target, level)
            
    def calculate(self):
        """Calculate cost, expense, and resources for this line."""
        self.expense = D('0.0')
        self.profit = D('0.0')
        self.resources = dict()
        for machine in self._machines:
            (cost, req, prod) = self.get_rp(machine)
            self.expense += cost
            for i in req:
                if i[0] is self.ds.Resources.cash:
                    self.expense += i[1]
                else:
                    self.resources[i[0]] = self.resources.get(i[0],
                                                              D('0.0')) - i[1]
            for i in prod:
                if i[0] is self.ds.Resources.cash:
                    self.profit += i[1]
                else:
                    self.resources[i[0]] = self.resources.get(i[0],
                                                              D('0.0')) + i[1]
                    
    def _deficit(self):
        """ Return first material we have a deficit of, or None """
        for k in self.resources.keys():
            if self.resources[k]<D('0.0'):
                return k
        return None

    def required(self, resource):
        """ Return total number of resource required by this line per tick """
        total = D('0.0')
        for m in self._machines:
            (_, req, _) = self.get_rp(m)
            for res in filter(lambda x: x[0]==resource, req):
                total += res[1]
        return total
    
    def get_rp(self, machine):
        """Return actual requirements and products of given machine."""
        el = self._efficiency_levels.get(machine, 0)
        tl = self._thrift_levels.get(machine, 0)
        rl = self._rate_levels.get(machine, 0)
        mvals = self.ds.mtypes[machine]
        try:
            em = mvals.efficiency[el]
            tm = mvals.thrift[tl]
            rm = mvals.rate[rl]
        except:
            print "GET_RP: {}".format(machine)
            raise
        cost = mvals.cost*tm*em*rm
        required = [(t, v*rm) for (t, v) in mvals.inputs]
        produced = [(t, v*rm*em) for (t, v) in mvals.outputs]
        return (cost, required, produced)

    def furnish(self):
        """Generate production line necessary to produce target."""
        self._machines = [self.target]
        self.calculate()
        debt = self._deficit()
        while debt is not None:
            self._machines.append(debt)
            self.calculate()
            debt = self._deficit()

    @property
    def machines(self):
        t = list()
        for r in self.ds.Resources:
            c = self._machines.count(r)
            if c > 0:
                t.append((r, c))
        return t

                
def report(line):
    rs =("{l1:20}: {prod}\n"
         "{l2:20}: {tmach}\n"
    ).format(l1='Product', l2='Total Machines',prod=line.target,
             tmach=sum([x[1] for x in line.machines]))
    for (m, c) in sorted(line.machines, key=lambda x: x[0].value, reverse=True):
        rs += "  {name:18}: {count}\n".format(name=m.name.capitalize(), count=c)
    rs += "Required resources:\n"
    for r in line.ds.Resources:
        tu = line.required(r)
        if tu > 0:
            rs += "  {name:18}: {count}\n".format(name=r.name.capitalize(),
                                                  count=tu)
    return rs


""" Example of using both the machine database, and the line generator """

from factory_calc.machines import Resources as R
import factory_idle as fi

levs = {
    'efficiency': {
        R.plastic: 1,
        R.steel: 1,
        R.electronic: 1,
        },
    'thrift': {
        R.coal: 2,
        R.oil: 2,
        R.gas: 2,
        R.silicon: 1,
        R.explosive: 1,
        R.aluminum: 2,
        R.steel: 2,
        R.plastic: 2,
        R.electronic: 1,
        R.bullet: 1,
        R.gun: 1,
        R.engine: 2,
        R.tankHull: 2,
        R.tankTurret: 2,
        R.tank: 2,
#        R.diesel: 1,
        },
    'rate': {
        R.ironOre: 7,
        R.coal: 6,
        R.oil: 4,
        R.gas: 5,
        R.silicon: 3,
        R.explosive: 2,
        R.aluminum: 3,
        R.iron: 6,
        R.steel: 4,
        R.plastic: 3,
        R.electronic: 2,
        R.bullet: 1,
        R.gun: 1,
        }
    }

def dotest():
    """ Generate a simple iron production line """
    l = fi.Line(R.ironSale)
    l.set_levels(levs)
    l.target=R.tankSale
    return l
